#include "vtkLidarPoseReader.h"
#include "vtkVelodynePosePacketInterpreter.h"
#include "vtkVelodyneLegacyPacketInterpreter.h"

#include <iostream>
#include <iomanip>

#include "TestHelpers.h"

const double epsilon = 1e-3;
bool check(std::string& PCAP,
           int modelType,
           bool PPSSynced,
           bool HasTimeshiftEstimation,
           double BaseTimeshift)
{
  auto reader = vtkSmartPointer<vtkLidarPoseReader>::New();
  auto lidarInterpreter = vtkSmartPointer<vtkVelodyneLegacyPacketInterpreter>::New();
  auto poseInterpreter = vtkSmartPointer<vtkVelodynePosePacketInterpreter>::New();
  lidarInterpreter->SetLidarModel(modelType);
  reader->SetLidarInterpreter(lidarInterpreter);
  reader->SetPoseInterpreter(poseInterpreter);
  reader->SetFileName(PCAP);
  reader->Modified();
  reader->Update();

  bool success = true;
  success &= (poseInterpreter->GetPPSSynced() == PPSSynced);
  success &= (poseInterpreter->GetHasTimeshiftEstimation() == HasTimeshiftEstimation);

  if (HasTimeshiftEstimation)
  {
    std::cout << std::fixed << std::setprecision(9)
              << "expected: " << BaseTimeshift
              << " got: " << poseInterpreter->GetTimeshiftEstimation()
                 - poseInterpreter->GetAssumedHardwareLag() << std::endl;
  }

  success &= !HasTimeshiftEstimation
                  || epsilon > std::abs((poseInterpreter->GetTimeshiftEstimation()
                                         - poseInterpreter->GetAssumedHardwareLag())
                                         - BaseTimeshift);
  return success;
}

int main(int argc, char* argv[])
{
  if (argc != 4)
  {
    std::cout << "Wrong number of arguments" << std::endl;
    return 1;
  }

  std::string PCAP_HDL32_no_PPS_sync = argv[1];
  std::string PCAP_VLP16_no_GPS_data = argv[2]; // (no GPS data implies no sync)
  std::string PCAP_VLP16_with_PPS_sync = argv[3];

  bool success = true;

  int VLP16Type = 0;
  int HDL32Type = 2;

  // First check in some order:
  success &= check(PCAP_HDL32_no_PPS_sync, HDL32Type, false, true, 75599.1423);
  success &= check(PCAP_VLP16_no_GPS_data, VLP16Type, false, false, 0.0);
  success &= check(PCAP_VLP16_with_PPS_sync, VLP16Type, true, true, 68399.9062);

  // Then check that the reader is resetted correctly when file is changed:
  success &= check(PCAP_VLP16_no_GPS_data, VLP16Type, false, false, 0.0);

  return success ? 0 : 1;
}
