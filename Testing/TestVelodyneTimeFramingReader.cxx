// Copyright 2020 Kitware SAS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "TestHelpers.h"
#include "vtkLidarReader.h"
#include "vtkVelodyneLegacyPacketInterpreter.h"

#include <vtkNew.h>
#include <vtkPointData.h>

#include <vtkInformation.h>
#include <vtkStreamingDemandDrivenPipeline.h>

/**
 * @brief This test does not require prediction values, thus the pcap should be easily changed.
 * @param pcapFileName The Velodyne pcap file containing points covering all 360° in plane
 * @param calibrationFileName The XML Velodyne sensor calibration file
 * @return 0 on success, 1 on failure
 */
int main(int argc, char* argv[])
{
  if (argc < 3)
  {
    std::cerr << "Wrong number of arguments. Usage: TestTimeFraming <pcapFileName> <calibrationFileName>" << std::endl;

    return 1;
  }

  const double frameDuration1 = 0.1;
  const double frameDuration2 = 0.2;

  std::string pcapFileName = argv[1];
  std::string calibrationFileName = argv[2];

  std::cout << "-------------------------------------------------------------------------" << std::endl
            << "Pcap :\t" << pcapFileName << std::endl
            << "Calibration:\t" << calibrationFileName << std::endl
            << "-------------------------------------------------------------------------" << std::endl;

  // Generate a Lidar reader
  vtkNew<vtkLidarReader> reader;
  auto interp = vtkSmartPointer<vtkVelodyneLegacyPacketInterpreter>::New();
  interp->SetIgnoreEmptyFrames(true);
  interp->SetIgnoreZeroDistances(true);
  interp->SetEnableAdvancedArrays(true);
  interp->SetCalibrationFileName(calibrationFileName.c_str());
  reader->SetLidarInterpreter(interp);
  reader->SetShowPartialFrames(true);
  reader->SetFileName(pcapFileName);
  reader->Update();

  int frameNumber = 0;
  vtkInformation* info = reader->GetOutputInformation(0);
  if (info->Has(vtkStreamingDemandDrivenPipeline::TIME_STEPS()))
  {
    frameNumber = info->Length(vtkStreamingDemandDrivenPipeline::TIME_STEPS());
  }

  // Check if we can get frames using the interpreter framing
  if (frameNumber == 0)
  {
      std::cerr << "error: the reader ouput 0 frame, please check paths and pcap file" << std::endl;
      return 1;
  }

  // Check that we can change the framing method
  reader->GetLidarInterpreter()->SetFramingMethod(NETWORK_PACKET_TIME_FRAMING);

  // Set First frame duration
  reader->GetLidarInterpreter()->SetFrameDuration_s(frameDuration1);
  reader->Update();
  int frameCount1 = 0;
  vtkInformation* info1 = reader->GetOutputInformation(0);
  if (info->Has(vtkStreamingDemandDrivenPipeline::TIME_STEPS()))
  {
    frameCount1 = info1->Length(vtkStreamingDemandDrivenPipeline::TIME_STEPS());
  }
  if (frameCount1 == 0)
  {
      std::cerr << "error: frames are expected when using NETWORK_PACKET_TIME_FRAMING" << std::endl;
      return 1;
  }

  // Check second frame duration
  reader->GetLidarInterpreter()->SetFrameDuration_s(frameDuration2);
  reader->Update();
  int frameCount2 = 0;
  vtkInformation* info2 = reader->GetOutputInformation(0);
  if (info->Has(vtkStreamingDemandDrivenPipeline::TIME_STEPS()))
  {
    frameCount2 = info2->Length(vtkStreamingDemandDrivenPipeline::TIME_STEPS());
  }
  if (frameCount2 == 0)
  {
      std::cerr << "error: frames are expected when using NETWORK_PACKET_TIME_FRAMING" << std::endl;
      return 1;
  }

  double length1 = frameDuration1 * frameCount1;
  double length2 = frameDuration2 * frameCount2;
  if (std::abs(length1 - length2) > 2.0 * std::max(frameDuration1, frameDuration2))
  {
      std::cerr << "error: frame count and frame duration do not change accordingly" << std::endl;
      return 1;
  }

  reader->UpdateInformation();
  vtkInformation* outInfo = reader->GetExecutive()->GetOutputInformation(0);
  double* timeSteps = outInfo->Get(vtkStreamingDemandDrivenPipeline::TIME_STEPS());
  double updateTime = timeSteps[1];
  outInfo->Set(vtkStreamingDemandDrivenPipeline::UPDATE_TIME_STEP(), updateTime);
  reader->Update();
  reader->GetOutput()->Register(NULL);
  vtkPolyData* frame = vtkPolyData::SafeDownCast(reader->GetOutput());

  double start = 0.0;
  double end = 0.0;

  if (!GetFrameTimeRange(frame, start, end))
  {
    std::cerr << "error: could not get frame time range" << std::endl;
    return 1;
  }

  double effectiveDuration = end - start;
  double relError = std::abs(effectiveDuration - frameDuration2) / frameDuration2;
  if (relError > 0.1)
  {
    std::cerr << "error: time range of points in frame differs too much from expected time range" << std::endl;
    return 1;
  }

  return  0;
}

