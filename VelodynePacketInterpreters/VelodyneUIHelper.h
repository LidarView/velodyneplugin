#ifndef VelodyneUIHelper_H
#define VelodyneUIHelper_H

#include <vtkAbstractArray.h>
#include <vtkNamedColors.h>
#include <vtkNew.h>
#include <vtkSmartPointer.h>

#ifdef vtkSMProxy_h
#include <vtkSMPropertyHelper.h>
#include <vtkSMProxyManager.h>
#include <vtkSMSessionProxyManager.h>
#include <vtkSMTransferFunctionManager.h>
#endif

/**
 * @brief ColorArrayAsIndexedLookupTable Color an array as an indexed table
 * @param array to colorize
 * @param values integers values used to define the caption
 * @param stringValues strings to print in the caption
 * @param UNUSED colors vector of the colors to use on the caption (colors[i] = {r, g, b}) 
 */
static void ColorArrayAsIndexedLookupTable(vtkSmartPointer<vtkAbstractArray> array,
                                           const std::vector<int>& values,
                                           const std::vector<std::string>& stringValues,
                                           const std::vector<vtkColor3<double>>& vtkNotUsed(colors))
{
  // array can be nullptr if its an advanced array
  if(!array)
  {
    return;
  }

  if(stringValues.size() != values.size())
  {
    std::cerr << "values in int and values in String should have the same size" << std::endl;
    return;
  }

#ifdef vtkSMPropertyHelper_h

  vtkSMSessionProxyManager* pxm = vtkSMProxyManager::GetProxyManager()->GetActiveSessionProxyManager();
  vtkNew<vtkSMTransferFunctionManager> mgr;

  vtkSMProxy* lutProxy = mgr->GetColorTransferFunction(array->GetName(), pxm);
  vtkSMPropertyHelper(lutProxy, "IndexedLookup", true).Set(1);

  if(values.size() > colors.size())
  {
    std::cout << "The number of colors is lower than the number of values" << std::endl
              << "Colors will be used multiples times" << std::endl;
  }

  for(unsigned int i = 0; i < values.size(); i++)
  {
    vtkSMPropertyHelper(lutProxy, "Annotations").Set(i * 2, std::to_string(values[i]).c_str());
    vtkSMPropertyHelper(lutProxy, "Annotations").Set((i * 2) + 1, stringValues[i].c_str());

    int colorIndex = i % colors.size();
    vtkSMPropertyHelper(lutProxy, "IndexedColors").Set((i * 3), colors[colorIndex].GetRed());
    vtkSMPropertyHelper(lutProxy, "IndexedColors").Set((i * 3) + 1, colors[colorIndex].GetGreen());
    vtkSMPropertyHelper(lutProxy, "IndexedColors").Set((i * 3) + 2, colors[colorIndex].GetBlue());
  }

#endif
}

/**
 * @brief ColorArrayAsIndexedLookupTable Color an array as an indexed table
 *                                       using a predefined colorMap
 * @param array to colorize
 * @param values integers values used to define the caption
 * @param stringValues strings to print in the caption
 */
static void ColorArrayAsIndexedLookupTable(vtkSmartPointer<vtkAbstractArray> array,
                                           const std::vector<int>& values,
                                           const std::vector<std::string>& stringValues)
{
  // Set a default color vectors to colorize each point category
  // This is inspirate from paraView/src/VTK/Common/Color/vtkNamedColors.cxx
  // We don't use it directly because vtkNew<vtkNamedColor> takes too much time
  std::vector<vtkColor3<double>> allColors;

  vtkColor3<double> green = vtkColor3<double>(0, 0.501, 0);
  allColors.push_back(green);

  vtkColor3<double> blue = vtkColor3<double>(0, 0, 1);
  allColors.push_back(blue);

  vtkColor3<double> red = vtkColor3<double>(1, 0, 0);
  allColors.push_back(red);

  vtkColor3<double> violet = vtkColor3<double>(0.933, 0.509, 0.933);
  allColors.push_back(violet);

  vtkColor3<double> Orange = vtkColor3<double>(1, 0.647, 0);
  allColors.push_back(Orange);

  vtkColor3<double> yellow = vtkColor3<double>(1, 1, 0);
  allColors.push_back(yellow);

  vtkColor3<double> grey = vtkColor3<double>(0.752, 0.752, 0.752);
  allColors.push_back(grey);

  vtkColor3<double> pink = vtkColor3<double>(1, 0.752, 0.796);
  allColors.push_back(pink);

  vtkColor3<double> cyan = vtkColor3<double>(0, 1, 1);
  allColors.push_back(cyan);

  vtkColor3<double> brown = vtkColor3<double>(0.647, 0.164, 0.164);
  allColors.push_back(brown);

  vtkColor3<double> white = vtkColor3<double>(1, 1, 1);
  allColors.push_back(white);

  vtkColor3<double> LawnGreen = vtkColor3<double>(0.486, 0.988, 0);
  allColors.push_back(LawnGreen);

  ColorArrayAsIndexedLookupTable(array, values, stringValues, allColors);
}

#endif // VelodyneUIHelper_H
