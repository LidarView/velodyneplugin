#ifndef VTKVELODYNELEGACYPACKETINTERPRETER_H
#define VTKVELODYNELEGACYPACKETINTERPRETER_H

#include "vtkVelodyneBasePacketInterpreter.h"

#include <vtkUnsignedCharArray.h>
#include <vtkUnsignedIntArray.h>
#include <vtkUnsignedShortArray.h>
#include <vtkUnsignedLongArray.h>
#include <vtkUnsignedLongLongArray.h>
#include <vtkStringArray.h>

#include <memory>

#include "VelodynePacketInterpretersModule.h"

using namespace DataPacketFixedLength;

class RPMCalculator;
class FramingState;
class vtkRollingDataAccumulator;


class VELODYNEPACKETINTERPRETERS_EXPORT vtkVelodyneLegacyPacketInterpreter : public vtkVelodyneBasePacketInterpreter
{
public:
  static vtkVelodyneLegacyPacketInterpreter* New();
  vtkTypeMacro(vtkVelodyneLegacyPacketInterpreter, vtkVelodyneBasePacketInterpreter)
  void PrintSelf(ostream& vtkNotUsed(os), vtkIndent vtkNotUsed(indent)) override {}; //TODO
  enum DualFlag
  {
    DUAL_DISTANCE_NEAR = 0x1,  // point with lesser distance
    DUAL_DISTANCE_FAR = 0x2,   // point with greater distance
    DUAL_INTENSITY_HIGH = 0x4, // point with lesser intensity
    DUAL_INTENSITY_LOW = 0x8,  // point with greater intensity
    DUAL_DOUBLED = 0xf,        // point is single return
    DUAL_DISTANCE_MASK = 0x3,
    DUAL_INTENSITY_MASK = 0xc,
  };

  void ProcessPacket(unsigned char const * data, unsigned int dataLength) override;

  bool SplitFrame(bool force = false, FramingMethod_t framingMethodAskingForSplitFrame = FramingMethod_t::INTERPRETER_FRAMING) override;

  bool IsLidarPacket(unsigned char const * data, unsigned int dataLength) override;


  void ResetCurrentFrame() override;

  bool PreProcessPacket(unsigned char const * data, unsigned int dataLength,
    double &outLidarDataTime) override;

  std::string GetSensorInformation(bool shortVersion = false) override;

  void SetLidarModel(int type);
  vtkGetMacro(LidarModel, int);

  vtkSetMacro(UseAutoCalibration, bool);
  vtkGetMacro(UseAutoCalibration, bool);

  vtkSetMacro(VLS128UseOldIntraFiringTiming, bool);
  vtkGetMacro(VLS128UseOldIntraFiringTiming, bool);

protected:

  vtkSmartPointer<vtkPolyData> CreateNewEmptyFrame(vtkIdType numberOfPoints, vtkIdType prereservedNumberOfPoints = 60000) override;

  // Process the laser return from the firing data
  // firingData - one of HDL_FIRING_PER_PKT from the packet
  // hdl64offset - either 0 or 32 to support 64-laser systems
  // firingBlock - block of packet for firing [0-11]
  // azimuthDiff - average azimuth change between firings
  // timestamp - the timestamp of the packet
  // geotransform - georeferencing transform
  void ProcessFiring(const HDLFiringData* firingData,
    int firingBlockLaserOffset, int firingBlock, int azimuthDiff, unsigned long long timestamp,
    unsigned int rawtime, bool isThisFiringDualReturnData, bool isDualReturnPacket, const HDLFiringData* extData, int extDataPacketType);

  void PushFiringData(unsigned char channelNumber, unsigned char channelNumberOr_dsrBase32_forVLP16,
                      unsigned short azimuth, const unsigned short elevation, const unsigned long long timestamp,
                      const unsigned int rawtime, const HDLLaserReturn* laserReturn,
                      const bool isFiringDualReturnData,
                      const int extDataPacketType, const HDLLaserReturn* extData);


  void Init();

  unsigned long long ComputeTimestamp(int nbrOfRollingTime, unsigned int tohTime);

  bool CheckReportedSensorAndCalibrationFileConsistent(const HDLDataPacket* dataPacket);

  vtkSmartPointer<vtkPoints> Points;
  vtkSmartPointer<vtkDoubleArray> PointsX;
  vtkSmartPointer<vtkDoubleArray> PointsY;
  vtkSmartPointer<vtkDoubleArray> PointsZ;
  vtkSmartPointer<vtkUnsignedCharArray> Intensity;
  vtkSmartPointer<vtkUnsignedCharArray> Drop;
  vtkSmartPointer<vtkStringArray> BinaryFlags;
  vtkSmartPointer<vtkUnsignedCharArray> Interference;
  vtkSmartPointer<vtkUnsignedCharArray> Confidence;
  vtkSmartPointer<vtkUnsignedCharArray> SunLevel;
  vtkSmartPointer<vtkUnsignedCharArray> RS; // Retro Shadow
  vtkSmartPointer<vtkUnsignedCharArray> RL; // Range Limited
  vtkSmartPointer<vtkUnsignedCharArray> RG; // Retro Ghost
  vtkSmartPointer<vtkUnsignedCharArray> LaserId;
  vtkSmartPointer<vtkUnsignedShortArray> Azimuth;
  vtkSmartPointer<vtkDoubleArray> Distance;
  vtkSmartPointer<vtkUnsignedShortArray> DistanceRaw;
  vtkSmartPointer<vtkUnsignedLongLongArray> Timestamp;
  vtkSmartPointer<vtkDoubleArray> VerticalAngle;
  vtkSmartPointer<vtkUnsignedIntArray> RawTime;
  vtkSmartPointer<vtkIntArray> IntensityFlag;
  vtkSmartPointer<vtkIntArray> DistanceFlag;
  vtkSmartPointer<vtkUnsignedIntArray> Flags;
  vtkSmartPointer<vtkIdTypeArray> DualReturnMatching;

  // sensor information
  int LidarModel;
  SensorType ReportedSensor;
  bool IsVLS128; // TODO: Replace this variable with LidarModel == VLS128
  bool alreadyWarnedForIgnoredHDL64FiringPacket;

  RPMCalculator* RpmCalculator_; // Packet dependant RPM calculator

  FramingState* CurrentFrameState;
  unsigned int CurrTimestamp;
  int LastAzimuth, LastAzimuthDiff;
  double TimeAdjust;
  vtkIdType LastPointId[HDL_MAX_NUM_LASERS];
  vtkIdType FirstPointIdOfDualReturnPair;
  bool UseAutoCalibration = true;
  bool VLS128UseOldIntraFiringTiming = false;

  unsigned char epororPowerMode;

  // Sensor parameters presented as rolling data, extracted from enough packets
  vtkRollingDataAccumulator* rollingCalibrationData;

  bool ShouldCheckSensor; // Used to check if first packet is consistent calibration with calibFile

  vtkVelodyneLegacyPacketInterpreter();
  ~vtkVelodyneLegacyPacketInterpreter();

private:
  vtkVelodyneLegacyPacketInterpreter(const vtkVelodyneLegacyPacketInterpreter&) = delete;
  void operator=(const vtkVelodyneLegacyPacketInterpreter&) = delete;

  bool IsNewFrame(const HDLDataPacket* dataPacket, int& firingToSkip);

  struct FrameInfo
  {
    //! Indicates the number of time rolled that has occured
    //! since the beginning of the .pcap file. hence, to have
    //! a non rolling timestamp one should add to the rolling
    //! timestamp NbrOfRollingTime * MaxTimeBeforeRolling
    int NbrOfRollingTime = 0;

    //! lastTohTimestamp save the last dataPacket->TohTimestamp
    //! This is used to increment NbrOfRollingTime
    //! when a rollback in dataPacket is detected
    uint32_t lastTohTimestamp = 0;

    void Reset()
    {
      this->NbrOfRollingTime = 0;
      this->lastTohTimestamp = 0;
    }
  };

  bool FirstPacket = true;
  FrameInfo PreParsingInfo;
  FrameInfo ParsingInfo;
};

#endif // VTKVELODYNELEGACYPACKETINTERPRETER_H
