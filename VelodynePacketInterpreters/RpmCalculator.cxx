#include "RpmCalculator.h"

#include <cmath>
#include <iostream>
#include <limits>

bool RPMCalculator::IsReady()
{
    return this->ready == Ready::ALL;
}

void RPMCalculator::ResetRPM()
{
  this->ready = Ready::NOT;
  this->limit_azm[0]  = std::numeric_limits<azimuth_t>::max();
  this->limit_azm[1]  = std::numeric_limits<azimuth_t>::min();
  this->limit_time[0] = std::numeric_limits<timestamp_t>::max();
  this->limit_time[1] = std::numeric_limits<timestamp_t>::min();
  this->invert = false;

}

void RPMCalculator::SetInvert(bool state)
{
  this->invert = state;
}

double RPMCalculator::GetRPM()
{
  // If the calculator is not ready i.e : one
  // of the attributes is not initialized yet
  // (limit_azm, limit_time)
  if (!this->IsReady())
  {
    return 0;
  }

  // delta angle in number of laps
  double dAngle = static_cast<double>(this->limit_azm[1] - this->limit_azm[0]) / (100.0 * 360.0);

  // delta time in minutes
  double dTime = static_cast<double>(this->limit_time[1] - this->limit_time[0]) / (60e6);

  // epsilon to test if the delta time / angle is not too small
  const double epsilon = 1e-12;

  // if one the deltas is too small
  if ((std::abs(dTime) < epsilon) || (std::abs(dAngle) < epsilon))
  {
    return 0;
  }

  if(!invert){
    return +std::ceil(dAngle / dTime);
  }else{
    return -std::ceil(dAngle / dTime);
  }
}

void RPMCalculator::AddData(azimuth_t angle, timestamp_t rawtime)
{
  if (angle < this->limit_azm[0])
  {
    this->limit_azm[0] = angle;
    this->ready |= AZIM_MIN;
  }
  if (angle > this->limit_azm[1])
  {
    this->limit_azm[1] = angle;
    this->ready |= AZIM_MAX;
  }
  if (rawtime < this->limit_time[0])
  {
    this->limit_time[0] = rawtime;
    this->ready |= TIME_MIN;
  }
  if (rawtime > this->limit_time[1])
  {
    this->limit_time[1] = rawtime;
    this->ready |= TIME_MAX;
  }
}

double RPMCalculator::ComputeFrequency(timestamp_t frame_curr)
{
  this->frame_diff = frame_curr - this->frame_last; // Diff timestamps betwwen spits, WIP if > amount do not register or set 0
  this->frame_last = frame_curr; //Save Timestamp at split

  constexpr float alpha = 0.05;
  if( this->frame_diff != 0)
  {
    this->Frequency = (alpha * std::ceil(1.0/(static_cast<double>(this->frame_diff)*1.0e-6)) ) + (1.0 - alpha) * this->Frequency;
  }else{
    this->Frequency = (1.0 - alpha) * this->Frequency;
  }

  return this->GetFrequency();
}

double RPMCalculator::GetFrequency()
{
  return this->Frequency;
}
