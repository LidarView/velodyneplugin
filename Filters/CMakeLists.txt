set(velodyne_classes
  vtkPlaneFitter
)

vtk_module_add_module(VelodynePlugin::Filters
  CLASSES ${velodyne_classes}
)

paraview_add_server_manager_xmls(
  MODULE VelodynePlugin::Filters
  XMLS
    PlaneFitter.xml
)
