-- trivial protocol example
-- declare our protocol
trivial_proto = Proto("VelodyneAPF","VelodyneAPF")
-- create a function to dissect it
function trivial_proto.dissector(buffer,pinfo,tree)
	pinfo.cols.protocol = "VelodyneAPF"
	local subtree = tree:add(trivial_proto,buffer(),"Velodyne APF data")

	local curr = 0

	local headerSize = 20
	local totalSize = buffer:len()

	-- Packet Header --
	local header_subtree = subtree:add(buffer(curr,headerSize),"Header")

	local VER_HLEN = buffer(curr,1)
	local VER = VER_HLEN:bitfield(0,4)
	local HLEN = VER_HLEN:bitfield(4,4)
	header_subtree:add(buffer(curr,1), "VER   : " .. VER)
	header_subtree:add(buffer(curr,1), "HLEN  : " .. HLEN)
	curr = curr + 1

	local NXHDR = buffer(curr, 1):uint()
	header_subtree:add(buffer(curr,1),"NXHDR : " .. NXHDR)
	curr = curr + 1

	local PTYPE_TLEN = buffer(curr,1)
	local PTYPE = PTYPE_TLEN:bitfield(0,4)
	local TLEN = PTYPE_TLEN:bitfield(4,4)
	header_subtree:add(buffer(curr,1), "PTYPE : " .. PTYPE)
	header_subtree:add(buffer(curr,1), "TLEN  : " .. TLEN)
	curr = curr + 1

	local MIC = buffer(curr, 1):uint()
	header_subtree:add(buffer(curr,1),"MIC   : " .. MIC)
	curr = curr + 1

	local PSEQ = buffer(curr, 4):uint()
	header_subtree:add(buffer(curr,4),"PSEQ  : " .. PSEQ)
	curr = curr + 4

	local TREF = buffer(curr, 8):uint64()
	header_subtree:add(buffer(curr,8),"TREF  : " .. TREF)
	curr = curr + 8

	local GLEN_FLEN = buffer(curr,1)
	local GLEN = GLEN_FLEN:bitfield(0,4)
	local FLEN = GLEN_FLEN:bitfield(4,4)
	header_subtree:add(buffer(curr,1), "GLEN : " .. GLEN)
	header_subtree:add(buffer(curr,1), "FLEN  : " .. FLEN)
	curr = curr + 1

	local DSET = buffer(curr,1)
	local DSET_BITDistance = DSET:bitfield(0,1)
	local DSET_Format = DSET:bitfield(1,1)
	local DSET_CountFormat_Value = DSET:bitfield(2,6)
	local DSET_MaskFormat_First_Return_Included = DSET:bitfield(7,1)
	local DSET_MaskFormat_Strongest_Return_Included = DSET:bitfield(6,1)
	local DSET_MaskFormat_Second_Strongest_Return_Included = DSET:bitfield(5,1)
	local DSET_MaskFormat_Last_Return_Included = DSET:bitfield(4,1)
	header_subtree:add(buffer(curr,1),"DSET_Format  : " .. DSET_Format)
	local NumberOfReturns = 0

	if DSET_Format==1
	then
		header_subtree:add(buffer(curr,1),"DSET_CountFormat_Value  : " .. DSET_CountFormat_Value)
		NumberOfReturns = DSET_CountFormat_Value
	else
		header_subtree:add(buffer(curr,1),"DSET_MaskFormat_First_Return_Included  : " .. DSET_MaskFormat_First_Return_Included)
		if DSET_MaskFormat_First_Return_Included ~= 0 then NumberOfReturns = NumberOfReturns + 1 end

		header_subtree:add(buffer(curr,1),"DSET_MaskFormat_Strongest_Return_Included  : " .. DSET_MaskFormat_Strongest_Return_Included)
		if DSET_MaskFormat_Strongest_Return_Included ~= 0 then NumberOfReturns = NumberOfReturns + 1 end

		header_subtree:add(buffer(curr,1),"DSET_MaskFormat_Second_Strongest_Return_Included  : " .. DSET_MaskFormat_Second_Strongest_Return_Included)
		if DSET_MaskFormat_Second_Strongest_Return_Included ~= 0 then NumberOfReturns = NumberOfReturns + 1 end

		header_subtree:add(buffer(curr,1),"DSET_MaskFormat_Last_Return_Included  : " .. DSET_MaskFormat_Last_Return_Included)
		if DSET_MaskFormat_Last_Return_Included ~= 0 then NumberOfReturns = NumberOfReturns + 1 end

	end
	
	curr = curr + 1

	local ISET = buffer(curr, 2)
	local ISET_Reflectivity = ISET:bitfield(15,1)
	local ISET_Intensity = ISET:bitfield(14,1)
	local ISET_Confidence = ISET:bitfield(13,1)
	local Numbers_Of_ISET_Values = 0

	header_subtree:add(buffer(curr,1),"ISET_Reflectivity  : " .. ISET_Reflectivity)
	if ISET_Reflectivity ~= 0 then Numbers_Of_ISET_Values = Numbers_Of_ISET_Values + 1 end

	header_subtree:add(buffer(curr,1),"ISET_Intensity  : " .. ISET_Intensity)
	if ISET_Intensity ~= 0 then Numbers_Of_ISET_Values = Numbers_Of_ISET_Values + 1 end

	header_subtree:add(buffer(curr,1),"ISET_Confidence  : " .. ISET_Confidence)
	if ISET_Confidence ~= 0 then Numbers_Of_ISET_Values = Numbers_Of_ISET_Values + 1 end

	curr = curr + 2



        -- Extension Header : If NXHDR = 0, the payload follow the current header, otherwise there is an extension header --
	while NXHDR ~= 0
	do 
	        local extHeader_HLEN = buffer(curr, 1):uint()
		
 		local extensionHeader = header_subtree:add(buffer(curr,extHeader_HLEN),"Firing Return : " ..extHeader)

		extensionHeader:add(buffer(curr,1),"extHeader_HLEN : " .. extHeader_HLEN)
		curr = curr + 1

		NXHDR = buffer(curr, 1):uint()
		extensionHeader:add(buffer(curr,1),"extHeader_NXHDR : " .. NXHDR)
		curr = curr + 1

        	-- Display all extension header data --
		for i =0, extHeader_HLEN-3
		do
			local extensionDataHeader = buffer(curr, 1):uint()
			extensionHeader:add(buffer(curr,1),"extension header data : " .. extensionDataHeader)
			curr = curr + 1
		end

	end

	local firing_group_header_Size = 8
	local totalFiringsSize = totalSize - headerSize - (4*TLEN)


	-- Firings --
	local AllFiringGroup = subtree:add(buffer(curr),"All Firing Group")
	local i =0
	while curr < totalFiringsSize
	do
		local firing_group_subtree = AllFiringGroup:add(buffer(curr),"Firing group : " ..i)
		i=i+1

		-- Firing group Header --
		local firing_group_header = firing_group_subtree:add(buffer(curr, firing_group_header_Size),"Firing group header")

		local TOFFS = buffer(curr,2):uint()
		firing_group_header:add(buffer(curr,2),"TOFFS  : " .. TOFFS)
		curr = curr + 2

		local FCNT_FSPN = buffer(curr,1)
		local FCNT = FCNT_FSPN:bitfield(0,5)
		local FSPN = FCNT_FSPN:bitfield(5,3)
		firing_group_header:add(buffer(curr,1), "FCNT : " .. FCNT)
		firing_group_header:add(buffer(curr,1), "FSPN  : " .. FSPN)
		curr = curr + 1

		local FDLY = buffer(curr,1):uint()
		firing_group_header:add(buffer(curr,1),"FDLY  : " .. FDLY)
		curr = curr + 1

		HDIR_VDIR_VDFL = buffer(curr,2)
		local HDIR = HDIR_VDIR_VDFL:bitfield(0,1)
		local VDIR = HDIR_VDIR_VDFL:bitfield(1,1)
		local VDFL = HDIR_VDIR_VDFL:bitfield(2,14)
		firing_group_header:add(buffer(curr,1), "HDIR : " .. HDIR)
		firing_group_header:add(buffer(curr,1), "VDIR : " .. VDIR)
		firing_group_header:add(buffer(curr,2), "VDFL : " .. VDFL)
		curr = curr + 2

		local AZM = buffer(curr,2):uint()
		firing_group_header:add(buffer(curr,2),"AZM  : " .. AZM)
		curr = curr+2

		local All_firing_subtree = firing_group_subtree:add(buffer(curr),"Firings")

		-- Firings : there is FCNT+1 firings in a group of firing --
		for j=0, FCNT
		do 
			local firing_subtree = All_firing_subtree:add(buffer(curr),"Firing : " ..j)
			
			-- Firing header --
			local firing_header = firing_subtree:add(buffer(curr,4),"Firing header")

			local LCN = buffer(curr,1):uint()
			firing_header:add(buffer(curr,1),"LCN  : " .. LCN)
			curr = curr + 1

			local FM_PWR = buffer(curr,1)
			local FM = PTYPE_TLEN:bitfield(0,4)
			local PWR = PTYPE_TLEN:bitfield(4,4)
			firing_header:add(buffer(curr,1), "FM : " .. FM)
			firing_header:add(buffer(curr,1), "PWR  : " .. PWR)
			curr = curr + 1

			local NF = buffer(curr,1):uint()
			firing_header:add(buffer(curr,1),"NF  : " .. NF)
			curr = curr + 1

			local STAT = buffer(curr,1):uint()
			firing_header:add(buffer(curr,1),"STAT  : " .. STAT)
			curr = curr + 1

			-- Return, the number of returns is determined by DSET value--
			for n=0, NumberOfReturns-1
			do
				local return_subtree = firing_subtree:add(buffer(curr),"Return : " ..n)

				-- Size of Distance encoding depends on DSET Format
				if DSET_BITDistance == 1
				then
					local Distance = buffer(curr,3):uint()
					return_subtree:add(buffer(curr,3),"Distance  : " .. Distance)
					curr = curr + 3
				else

					local Distance = buffer(curr,2):uint()
					return_subtree:add(buffer(curr,2),"Distance  : " .. Distance)
					curr = curr + 2
				end

				if ISET_Reflectivity == 1
				then
					local Reflectivity = buffer(curr,1):uint()
					return_subtree:add(buffer(curr,1),"Reflectivity  : " .. Reflectivity)
					curr = curr + 1
				end

				if ISET_Intensity == 1
				then
					local Intensity = buffer(curr,1):uint()
					return_subtree:add(buffer(curr,1),"Intensity  : " .. Intensity)
					curr = curr + 1
				end

				if ISET_Confidence == 1
				then
					local Confidence = buffer(curr,1):uint()
					return_subtree:add(buffer(curr,1),"Confidence  : " .. Confidence)
					curr = curr + 1
				end


			end

		end

	end

end


-- load the udp.port table
udp_table = DissectorTable.get("udp.port")
-- register our protocol to handle udp port 7777
udp_table:add(2368,trivial_proto)
